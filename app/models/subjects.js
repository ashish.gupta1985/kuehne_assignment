module.exports = (sequelize, type) => {
    return sequelize.define('subjects', {
            id: {
                type: type.STRING,
                primaryKey: true
            },
            code: {
                type: type.STRING
            },
            name: {
                type: type.STRING
            }
        }
    )
}