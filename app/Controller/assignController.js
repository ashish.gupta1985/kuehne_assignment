const express = require('express');
const router = express.Router()
const { response } = require('../utilities/responseStructure');
const { getFilteredRecords } = require("../services/assignService");
/**
 * Return  data of Subjects table after applying required filters
 */
router.get('/', (req, res, next) => {
    try {
        getFilteredRecords(req.body,(error, result, query)=>{
            response(req, error, result, query, (rs) => {
                res.status(rs.status).json(rs.json);
            });
        });
    } catch (error) {
        res.status(400).json(error);
    }
});

module.exports = router;