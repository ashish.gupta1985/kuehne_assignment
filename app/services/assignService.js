const { subjects } = require('../config/sequelize');
const path = require("path");
const soap = require('soap');

const WSDL = path.join(__dirname, './../../../../wsdl', `${process.env.WSDL}`);

const wsdlOptions = { useEmptyTag: true };

/**
 * get All records from mysql Table
 * Filter them as per business rule
 * Makes a SOAP request to external service
 * @param data
 * @param callback
 */
function getFilteredRecords(data, callback) {
    try {
        let where = {};
        subjects.findAll(where)
            .then(res => {
                let filteredArray = res.filter(function(el) {
                    return typeof el.id !== 'undefined' &&
                        el.id !== null &&
                        el.id !== "" &&
                        el.code === "025" &&
                        el.name.includes("test");
                });
                //TODO: integrate SOAP service details before uncomment
               // callSoapService(filteredArray,callback);
                callback(null, filteredArray);
            })
            .catch(error => { callback(error); });
    } catch (error) {
        callback(error);
    }
}

/**
 * Client method to make request to SOAP service
 * @param data
 * @param callback
 */
function callSoapService(data, callback) {
    try{
        soap.createClient(WSDL, wsdlOptions, (err, client) => {
            if (err) {
                callback(err);
            }
            client.saveData(data, (error, response) => {
                if(error){
                    callback(error)
                }
                callback(null, response);
            })
        });
    }catch (error){
        callback(error)
    }
}

module.exports = {
    getFilteredRecords
}