const express = require('express');
const cors = require('cors');
const app = express();
require('./config/sequelize');
const assignRoutes = require('../app/Controller/assignController');

app.use(cors());

// Handling CROS origin error
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers','Content-Type, Accept, Authorization');
    res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET');

    if (req.method === "OPTIONS") {
        return res.status(200).end();
    }

    return next();
});

//routes
app.use('/assign', assignRoutes);

app.use((req, res, next) => {
    res.status(404).send({message:'not found'});
})


// Handling server errors
app.use((error, req, res, next) => {
    res.status(500).send(error);
});

module.exports = app;