const Sequelize = require("sequelize");
const subjectsModel = require("../models/subjects");
const {
    DATABASE_NAME,
    USER,
    PASSWORD,
    HOST,
    DIALECT,
} = require("./databaseConnection");

const sequelize = new Sequelize (DATABASE_NAME, USER, PASSWORD, {
    host: HOST,
    dialect: DIALECT,
    pool: {
        max: 10,
        min: 0,
        acquire: 30000,
        idle: 10000,
    },
});

const subjects = subjectsModel(sequelize, Sequelize);

sequelize
    .sync({
        force: false,
        //  alter : true
    })
    .then(() => {
        console.log(`Database & tables created here!`);
    })
    .catch((err) => {
        console.log("Error------", err);
    });

module.exports = {
    subjects
}